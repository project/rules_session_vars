<?php

namespace Drupal\Tests\rules_session_vars\Kernel;

use Drupal\rules\Context\ContextConfig;
use Drupal\Tests\rules\Kernel\RulesKernelTestBase;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Test using the Rules API to create and evaluate session tests.
 *
 * @group rules_session_vars
 */
class RulesSessionTest extends RulesKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['rules_session_vars'];

  /**
   * Tests session rules.
   */
  public function testSessionRules() {
    $request_stack = $this->container->get('request_stack');
    $session = $this->prophesize(SessionInterface::class);
    $session->get('test_key')->willReturn('test_value');
    $request_stack->getCurrentRequest()->setSession($session->reveal());

    // Session exists.
    $condition = $this->expressionManager->createAnd()
      ->addCondition('session_key_exists',
        ContextConfig::create()
          ->setValue('session_key', 'test_key')
      );
    $this->assertTrue($condition->execute());

    // Session equals.
    $condition = $this->expressionManager->createAnd()
      ->addCondition('session_data_comparison',
        ContextConfig::create()
          ->setValue('session_key', 'test_key')
          ->setValue('operation', '==')
          ->setValue('session_value', 'test_value')
      );
    $this->assertTrue($condition->execute());
  }

}
