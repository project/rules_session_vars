<?php

namespace Drupal\rules_session_vars\Plugin\Condition;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rules\Plugin\Condition\DataComparison;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a 'Session Data Comparison' condition.
 *
 * @Condition(
 *   id = "session_data_comparison",
 *   label = @Translation("Session data comparison"),
 *   category = @Translation("Rules session vars"),
 *   context_definitions = {
 *     "session_key" = @ContextDefinition("any",
 *       label = @Translation("Session key"),
 *       description = @Translation("Session key to identify your data.")
 *     ),
 *     "operation" = @ContextDefinition("string",
 *       label = @Translation("Operator"),
 *       description = @Translation("The comparison operator. Valid values are == (default), <, >, CONTAINS (for strings or arrays) and IN (for arrays or lists)."),
 *       assignment_restriction = "input",
 *       default_value = "==",
 *       required = FALSE
 *     ),
 *     "session_value" = @ContextDefinition("any",
 *       label = @Translation("Session value"),
 *       description = @Translation("The value to compare the session data with.")
 *     ),
 *   }
 * )
 */
class SessionDataComparison extends DataComparison implements ContainerFactoryPluginInterface {

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface|null
   */
  protected $session;

  /**
   * Constructor for SessionDataComparison.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Symfony\Component\HttpFoundation\Request|null $request
   *   The request.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Request $request = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->session = $request ? $request->getSession() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function doEvaluate($sessionKey, $operation, $value) {
    if ($this->session && ($data = $this->session->get($sessionKey))) {
      return parent::doEvaluate($data, $operation, $value);
    }
    // No SESSION data, so fail evaluation.
    return FALSE;
  }

}
