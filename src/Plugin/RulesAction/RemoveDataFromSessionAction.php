<?php

namespace Drupal\rules_session_vars\Plugin\RulesAction;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rules\Core\RulesActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a 'Remove data from session' action.
 *
 * @RulesAction(
 *   id = "remove_data_from_session",
 *   label = @Translation("Remove value from $_SESSION"),
 *   category = @Translation("Session"),
 *   context_definitions = {
 *     "session_key" = @ContextDefinition("string",
 *       label = @Translation("Key to identify your session data")
 *     )
 *   }
 * )
 */
class RemoveDataFromSessionAction extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface|null
   */
  protected $session;

  /**
   * Constructor for RemoveDataFromSessionAction.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Symfony\Component\HttpFoundation\Request|null $request
   *   The request.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Request $request = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->session = $request ? $request->getSession() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecute($data_key) {
    if ($this->session) {
      $this->session->remove($data_key);
    }
  }

}
